# Show Me the Weather!

## Project Goal

The goal of this project is to create a scalable system that will return the weather conditions of a location that is requested. I took the approach of making this project completely cloud based using AWS and Azure. I also wanted a CI/CD pipeline to automate the process of testing and deploying my code. A feature that I am adding is a way for a client to request a SMS and/or email of the weather to themselves. 

## Project Scope
#### Deliverables ####
- API that communicates with a OpenWeatherMap API.
- Client application that makes request to the API and displays weather conditions on a webpage.
- Serverless Lambda Functions that process requests for sending email and SMS.
- DynamoDb Tables to store these requests for reporting.
- Unit testing for each software component.
- A CI/CD system using Bitbucket and AWS Codepipeline.
#### Exclusions ####
- Authentication flow for API and Client App using oAuth2.0
- Multiple API endpoints to provide different weather information such as 5 day forecasts or historical weather.

## Solution Architecture##

![architecture](https://dev-webmvc-dotnet.azurewebsites.net/Images/architecture.png){:height="50%" width="50%"}

#### WebAPI - Client Application####

 - [dev-webapi-dotnet](https://bitbucket.org/ConorCroke21/dev-webapi-dotnet/src/master/) - Click here to find more info
 - [dev-webmvc-dotnet](https://bitbucket.org/ConorCroke21/dev-webmvc-dotnet/src/master/) - Click here to find more info


#### API Gateway - StepFunction - Lambda####

![stepfunction](https://dev-webmvc-dotnet.azurewebsites.net/Images/stepfunction.png){:height="50%" width="50%"}

The mechanism for sending SMS and Email are split into two Lambda functions. With step functions I am able to send SMS and Email in parallel with only one request to the Api Gateway. This step function also logs each message in its relevant DynamoDB Table.

It is split into several stateless applications

- [dev-sendsms-lambda-dotnet](https://bitbucket.org/ConorCroke21/dev-sendsms-lambda-dotnet/src/master/) - This Lambda Function is written in C# and makes request to Twilio to send an SMS.
- [dev-sendemail-lambda-py](https://bitbucket.org/ConorCroke21/dev-sendemail-lambda-py/src/master/) -This Lambda Function is written in Python and makes a request to send an email through AWS SES.
- [dev-putitem-smsdb-lambda-py](https://bitbucket.org/ConorCroke21/dev-putitem-smsdb-lambda-py/src/master/) - This Lambda Function is written in Python and puts an Item in the OutboundSMS DynamoDB.
- [dev-putitem-emaildb-lambda-py](https://bitbucket.org/ConorCroke21/dev-putitem-emaildb-lambda-py/src/master/) -This Lambda Function is written in Python and puts an Item in the OutboundEmail DynamoDB.


 


## CI/CD Flow##
![codepipeline](https://dev-webmvc-dotnet.azurewebsites.net/Images/codepipeline.png){:height="50%" width="50%"}

Both the Lambda Functions and ASP.NET WebAPI have a full CI/CD flow with testing, building and deploymet.
#### Git Commit History ####
![git](https://dev-webmvc-dotnet.azurewebsites.net/Images/webapigit.PNG){:height="50%" width="50%"}

The history of each program can be seen in the Commit History, this includes branches which were used to add new features and after testing were pulled into master to be deployed.

#### AWS Pipeline ####
![pipeline](https://dev-webmvc-dotnet.azurewebsites.net/Images/pipeline.PNG){:height="50%" width="50%"}
I used AWS Pipeline to Build and Deploy my programs. Anytime there was a changed detected in the master branch of a code commit, AWS would automatically build and deploy the program with a full history of every step. Tests were ran at build stage to ensure no errors would make it to deployment.

